package com.example.demo.song.excel.controller;

import com.example.demo.song.excel.domain.ExcelMap;
import com.example.demo.song.excel.domain.ExcelMapInterface;
import com.example.demo.song.excel.domain.Product;
import com.example.demo.song.excel.service.ExcelReadService;
import lombok.AllArgsConstructor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.formula.functions.T;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Controller
public class ExcelTestController {
    //https://github.com/heowc/SpringBootSample
    @Autowired
    private ExcelReadService excelReadService;

    @RequestMapping(value = "/")
    public String index() {
        return "excel-sample";
    }

    @PostMapping("/file-test")
    public String handleFileUpload(@RequestParam("file") MultipartFile multipartFile,
                                   RedirectAttributes redirectAttributes) throws IOException, InvalidFormatException{

        List<Map<String, Object>> excelList = excelReadService.readFileToList(multipartFile, this::from);
        System.out.println(excelList);

        return "redirect:/";
    }

    //UI에서 만들어서 던져도 되고, 컴포넌트에서 동적으로 만들어도 될 듯 하다.
    private Map<String, Object> from(Row row) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", row.getCell(0).getStringCellValue() );
        map.put("pw", row.getCell(1).getStringCellValue() );
        map.put("test", row.getCell(2).getStringCellValue() );
        return map;
    }


}
