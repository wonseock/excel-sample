package com.example.demo.song.excel.domain;

import org.apache.poi.ss.usermodel.Row;

public interface ExcelMapInterface {

    ExcelMap from(Row row);
}
